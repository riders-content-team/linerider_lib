import rospy
import math
import time
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from linerider_description.srv import CameraSensor, LineriderControl
from std_srvs.srv import Trigger
import os
import requests
import time

class Robot:
    def __init__(self, robot_name):
        rospy.init_node("linerider_controller")

        self.robot_cmd_vel = rospy.Publisher("/linerider/cmd_vel", Twist, queue_size=10)

        self.image = self._Image()

        self._init_robot_control()
        self._init_sensor_services()

        time.sleep(0.5)

        self.initial_speed = 0
        self.ach4_test_flag = False

        self.ach1_flag = False
        self.ach2_flag = False
        self.ach3_flag = False
        self.ach4_flag = False

        self.linear_speed = 0
        self.angular_speed = 0
        self.step_size = 10

        self._init_game_controller()

    def _init_robot_control(self):
        try:
            rospy.wait_for_service("/linerider_control", 1)

            self.control = rospy.ServiceProxy('/linerider_control', LineriderControl)
            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))

            return False

    def _init_sensor_services (self):
        try:
            rospy.wait_for_service("/front_camera/camera_service", 0.5)

            self.image_data_service = rospy.ServiceProxy('/front_camera/camera_service', CameraSensor)

            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            self.image.convert_str_to_rgb(resp.data)

            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))

            return False

    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)

            resp = self.robot_handle()
            self.status = resp.message

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"

            rospy.logerr("Service call failed: %s" % (e,))

    def _check_game_controller(self):
        resp = self.robot_handle()
        self.status = resp.message

    def move(self, linear_speed):

        if not self.ach1_flag:
            self.achieve(75)
            self.ach1_flag = True

        if not self.ach4_test_flag:
            self.initial_speed = linear_speed
            self.ach4_test_flag = True

        if not self.ach4_flag and self.ach4_test_flag:
            if (not self.initial_speed == linear_speed):
                self.achieve(78)
                self.ach4_flag = True

        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "stop":
                return

            self.linear_speed = linear_speed
            if self.control:
                self.control(self.linear_speed, self.angular_speed, self.step_size)
            else:
                return

    def rotate(self, angular_speed):
        if not self.ach3_flag:
            self.achieve(77)
            self.ach3_flag = True

        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "stop":
                return

            self.angular_speed = angular_speed
            if self.control:
                self.control(self.linear_speed, self.angular_speed, self.step_size)
            else:
                return

    def get_sensor_data(self):
        image = self.image_data()
        sensor_row = []
        for i in range(image.width):
            brightness = (0.2126 * ord(image.data[i * 3])) + (0.7152 * ord(image.data[i * 3 + 1])) + (0.0722 * ord(image.data[i * 3 + 2]))
            sensor_row.append(brightness)
        return sensor_row

    def image_data(self):
        if not self.ach2_flag:
            self.achieve(76)
            self.ach2_flag = True

        resp = self.image_data_service()

        self.image.data = resp.data

        return self.image


    def is_ok(self):
        if not rospy.is_shutdown():
            self.image_data()
            return True
        else:
            return False

    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []

        def convert_str_to_rgb(self, data):
            self.data = []

            for i in range(self.height * self.width * 3):
                self.data.append(ord(data[i]))

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })
