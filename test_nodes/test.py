#!/usr/bin/env python
from linerider_lib.linerider import Robot
import rospy
import math

robot = Robot("linerider")

#Your code starts here:
#-1 right 1 left
last_rotation = -1

while(robot.is_ok()):
    image = robot.image_data()

    sensor_row = robot.get_sensor_data()

    # find the center
    max = sensor_row[0]
    index = 0
    for i in range(0, len(sensor_row)):
        if sensor_row[i] > max:
            max = sensor_row[i]
            index = i

    # calculate a deviation from line
    error = index - (image.width / 2)

    if error > 2 or error < -2:
        robot.rotate(1*last_rotation)
    else:
        robot.move(0.8)
        pass

    robot.rotate(-error*0.2)
    if error < 0:
        last_rotation = 1
    else:
        last_rotation = -1
